package com.bysocket.constant;

public interface SearchConstant {

    // IK 分词
    String  TOKENIZER_IK_MAX   = "ik_max_word";
    Integer SEARCH_TERM_LENGTH = 1;   // 如果搜索词大于 1 个字，则经过 IK 分词器获取分词结果列表
    String  STRING_TOKEN_SPLIT = ","; // 搜索词列表，按中英文，分割的字符串

    // 搜索
    Integer MINIMUM_SHOULD_MATCH = 1;
    String  INDEX_NAME = "bysocket";

    // 筛选
    String TYPE_NAME = "type";
    String CATEGORY_NAME = "category";
}
