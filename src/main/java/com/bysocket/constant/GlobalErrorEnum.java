package com.bysocket.constant;

public enum GlobalErrorEnum {

    PARAMS_NOT_COMPLETE(10000, "参数不完整"),
    PARAMS_ILLEGAL(10001, "参数不合法"),
    RESOURCE_NOT_EXISTS(10002, "资源不存在");

    private int code;

    private String message;

    GlobalErrorEnum(int code, String message){
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
