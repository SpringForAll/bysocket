package com.bysocket.constant;

public enum AdminEnum {

    PARAMS_NOT_COMPLETE(1, "Spring For All 社区");

    private int code;

    private String message;

    AdminEnum(int code, String message){
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
