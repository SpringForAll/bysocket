# bysocket 系统
AI For All

## 接口

### 批量新增接口

```
POST /api/contents HTTP/1.1
Host: 127.0.0.1:8080
Content-Type: application/json
Cache-Control: no-cache
Postman-Token: 87852ada-f13f-e300-d986-f02f26715643

[
	{
    	"id":1,
    	"title":"Spring Cloud中Feign如何统一设置验证token",
    	"content":"原理是通过每个微服务请求之前都从认证服务获取认证之后的token,然后将token放入到请求头中带过去，这样被调用方通过验证token来判断是否合法请求",
    	"type":"2",
    	"updateTime":null,
    	"category":3,
    	"read":666,
    	"support":777
	},
	{
    	"id":4,
    	"title":"Spring Cloud Zuul结合Smconf配置中心动态进行IP黑名单限制",
    	"content":"API网关中最常见的一个功能就是防止爬虫，当然防止爬虫的前提是先得识别出这个请求是爬虫",
    	"type":"5",
    	"updateTime":null,
    	"category":6,
    	"read":12,
    	"support":33
	}
]
```

### 搜索接口

```
POST /api/content/search HTTP/1.1
Host: 127.0.0.1:8080
Content-Type: application/json
Cache-Control: no-cache
Postman-Token: affe2cfc-bae0-274f-272f-8beef613a5a3

{
	"pageNumber":0,
	"pageSize":10,
	"searchContent":"微服务",
	"type": 2,
	"category":3,
	"orderName":"read" // 排序字段：updateTime、read、support
}
```
